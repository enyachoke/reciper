<?php
require('rb.php');
include_once __DIR__.'/Reciper/Reciper.php';
function Worker($urls,$cat,$cat_no){
$recipeEngine = new Reciper();
R::setup('mysql:host=localhost;
        dbname=recipes','root','geeks2012');
R::freeze(true);
// Sample URLs
//$urls[]='http://www.whats4eats.com/vegetables/irio-recipe';
//$urls[]='http://www.whats4eats.com/soups/matoke-recipe';
//$url = 'http://www.whats4eats.com/vegetables/irio-recipe';
foreach ($urls as $url){
 $recette = $recipeEngine->getRecipe($url);   
  $recipe = R::dispense('recipe');
  $recipe->title=$recette->getTitle();
  $recipe->desctitle=$recette->getDesc_title();
  $recipe->servings=$recette->getServings();
  $recipe->thumb= $recette->getThumb_url();
  $recipe->teaser=$recette->getTeaser();
   $counter = 1;
   foreach(preg_split("/((\r?\n)|(\r\n?))/", $recette->getIngredients()) as $line){
     ${"ingredient_{$counter}"}=R::dispense('ingredient');
     ${"ingredient_{$counter}"}->name    = $line;
     $recipe->ownIngredient[]= ${"ingredient_{$counter}"};
     $counter++;
}
$counter=0;
 foreach(preg_split("/((\r?\n)|(\r\n?))/", $recette->getInstructions()) as $line){
     ${"instruction_{$counter}"}=R::dispense('instruction');
     ${"instruction_{$counter}"}->name    = $line;
     $recipe->ownInstruction[]= ${"instruction_{$counter}"};
     $counter++;
} 
  $id = R::store($recipe);
   $category = R::load('category',$cat_no);
   $category->name=$cat;
   $category->ownRecipe[] =$recipe;
   R::store($category);
}
}
