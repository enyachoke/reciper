<?php
require_once __DIR__.'/Crawlers/whats4eatsCrawler.php';

class Reciper{
private $crawlers;

public function __construct(){
$this->crawlers = array (
'whats4eats.com' => new whats4eatsCrawler(),
);
}

public function getRecipe($url){
$crawler = $this->getCrawlerFromUrl($url);
if ($crawler){
return $crawler->crawl($url);
}

return null;
}

private function getCrawlerFromUrl($url){
foreach ($this->crawlers as $host => $crawler) {
if (strstr($url, $host))
return $crawler;
}

return null;
}
}