<?php
require_once __DIR__.'/Recipe.php';
require_once __DIR__.'/AbstractRecipeCrawler.php';
/*
Recipe crawler for the recipes on the website "CuisineAZ"
*/
class whats4eatsCrawler extends AbstractRecipeCrawler{
function crawl($url){
$recipe = new Recipe();

$this->getPage($url);
$page = $this->crawler->filter('.center-inner')
;
$pageContent = $page->filter('.content');

// Gets the title
$recipe->setTitle($this->getTextValue($page->filter('h1')));
//Gets Descriptive title
$recipe->setDesc_title($this->getTextValue($page->filter('div.field-item.odd p')));
//Gets Teaser
$recipe->setTeaser($this->getTextValue($page->filter('p#teaser')));
//Gets image url
$img='';
$imgs = $pageContent->filter('a img.image.image-thumbnail');
 foreach($imgs as $img) {
    $image=$img->getAttribute('src'); //$img is an instance of PHP's DOMElement 
}
 //print($image);
 $recipe->setThumb_url($image);
//Gets the servings
$recipe->setServings($this->getTextValue($pageContent->filter('p#servings i')));
// Get the ingredients
$ingr_list = $pageContent->filter('#ingredients ul li');
$recipe->setIngredients($this->convertElementsIntoString($ingr_list));
// Retrieve the instructions
$instruction_list = $pageContent->filter('#method ol li');
$recipe->setInstructions($this->convertElementsIntoString($instruction_list));
return $recipe;
}
}