<?php
/*
Recipe data structure, which holds all the necessary information
*/
class Recipe
{
protected $title;
protected $ingredients;
protected $instructions;
protected $servings;
protected $desc_title;
protected $teaser;
protected $thumb_url;
public function __construct(){
$this->setTitle('');
$this->setIngredients('');
$this->setInstructions('');
$this->setServings('');
$this->setDesc_title('');
$this->setTeaser('');
$this->setThumb_url('');
}

///////////////////////////////////////
/// Getters
///////////////////////////////////////
public function getTitle(){
return $this->title;
}

public function getIngredients(){
return $this->ingredients;
}

public function getInstructions(){
return $this->instructions;
}

public function getServings(){
return $this->servings;
}

public function getDesc_title(){
return $this->desc_title;
}
public function getTeaser(){
return $this->teaser;
}
public function getThumb_url(){
return $this->thumb_url;
}
///////////////////////////////////////
/// Setters
///////////////////////////////////////
public function setTitle ($title){
$this->title = $title;
}

public function setIngredients($ingredients){
$this->ingredients = $ingredients;
}

public function setInstructions($instructions){
$this->instructions = $instructions;
}

public function setServings($servings){
$this->servings = $servings;
}

public function setDesc_title($desc_title){
$this->desc_title= $desc_title;
}
public function setTeaser($teaser){
$this->teaser = $teaser;

}
public function setThumb_url($thumb_url){
$this->thumb_url = $thumb_url;
}
}


